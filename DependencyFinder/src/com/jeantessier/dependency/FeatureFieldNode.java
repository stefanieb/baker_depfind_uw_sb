package com.jeantessier.dependency;

public class FeatureFieldNode extends FeatureNode {

	private String _vis;

	public FeatureFieldNode(ClassNode parent, String name, boolean concrete) {
		super(parent, name, concrete);
	}

	@Override
	public NodeFactory.FeatureKind getKind() {
		return NodeFactory.FeatureKind.FIELD;
	}

	
}
