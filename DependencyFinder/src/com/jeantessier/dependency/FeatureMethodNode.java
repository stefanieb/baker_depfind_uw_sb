package com.jeantessier.dependency;

import java.util.List;
import java.util.Vector;

public class FeatureMethodNode extends FeatureNode {

	private String _returnTypeName = null;
	private List<String> _paramaters = new Vector<String>();

	public FeatureMethodNode(ClassNode parent, String name, boolean concrete) {
		super(parent, name, concrete);
	}

	@Override
	public NodeFactory.FeatureKind getKind() {
		return NodeFactory.FeatureKind.METHOD;
	}

	public void setReturnTypeName(String returnTypeName) {
		_returnTypeName = returnTypeName;
	}

	public String getReturnTypeName() {
		return _returnTypeName;
	}

	public void setParameters(List<String> params) {
		_paramaters = params;
	}

	public List<String> getParameters() {
		return _paramaters;
	}
}
