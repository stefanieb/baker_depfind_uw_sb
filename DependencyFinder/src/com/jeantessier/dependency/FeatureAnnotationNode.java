package com.jeantessier.dependency;

public class FeatureAnnotationNode extends FeatureNode {

	private String _annotationTypeName;

	public FeatureAnnotationNode(ClassNode parent, String name, boolean concrete) {
		super(parent, name, concrete);
	}
	
	@Override
	public NodeFactory.FeatureKind getKind() {
		return NodeFactory.FeatureKind.ANNOTATION;
	}
	
	// public void setAnnotationTypeName(String returnTypeName) {
	// _annotationTypeName = returnTypeName;
	// }
	//
	// public String getAnnotationTypeName() {
	// return _annotationTypeName;
	// }

}
