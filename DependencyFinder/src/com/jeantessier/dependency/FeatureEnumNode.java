package com.jeantessier.dependency;

public class FeatureEnumNode extends FeatureNode {

	public FeatureEnumNode(ClassNode parent, String name, boolean concrete) {
		super(parent, name, concrete);
	}

	@Override
	public NodeFactory.FeatureKind getKind() {
		return NodeFactory.FeatureKind.ENUM;
	}
}
