/*
 * Structure adapted from Jean Tessier's XMLPrinter class.
 * 
 * Reid Holmes
 */

/*
 *  Copyright (c) 2001-2009, Jean Tessier
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *  
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *  
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *  
 *      * Neither the name of Jean Tessier nor the names of his contributors
 *        may be used to endorse or promote products derived from this software
 *        without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * 
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.jeantessier.dependency;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import ca.uwaterloo.cs.se.inconsistency.core.model2.AbstractElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.AnnotatableElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.ClassElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.FieldElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.MethodElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.MethodParamElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.MethodReturnElement;
import ca.uwaterloo.cs.se.inconsistency.core.model2.Model;



public class UWModelExtractor extends Printer {
	public static final String DEFAULT_ENCODING = "utf-8";
	public static final String DEFAULT_DTD_PREFIX = "http://depfind.sourceforge.net/dtd";

	private boolean atTopLevel = false;

	private boolean VERBOSE_OUTPUT = true;
	private Model _model = new Model();

	public UWModelExtractor(PrintWriter out) {
		super(out);
	}

	public void traverseNodes(Collection<? extends Node> nodes) {
		if (atTopLevel) {
			super.traverseNodes(nodes);
		} else {
			atTopLevel = true;

			super.traverseNodes(nodes);

			// calls
			for (AbstractElement ae : _elementToNodeMap.keySet()) {
				if (ae instanceof MethodElement) {
					MethodElement me = (MethodElement) ae;
					Node fn = _elementToNodeMap.get(me);

					for (Node node : fn.getOutboundDependencies()) {
						if (node instanceof FeatureMethodNode) {

							MethodElement target = (MethodElement) _stringToElementMap.get(node.getName());

							// System.out.println("Call found: <call s=\"" + me.getId() + "\" t=\"" + node.getName() + "\"/>");

							if (!me.getCalls().contains(target))
								me.getCalls().add(target);
							// indent().append("<call s=\"" + me.getId() + "\" t=\"" + node.getName() + "\"/>").eol();
						}
					}
				}
			}

			// references
			for (AbstractElement ae : _elementToNodeMap.keySet()) {
				if (ae instanceof FieldElement) {
					FieldElement fe = (FieldElement) ae;
					Node fn = _elementToNodeMap.get(fe);

					// this is the opposite of the others; instead of looking at outbound dependencies (e.g., who I
					// call), we're looking at inbound (e.g., who calls me). this makes the sort not quite right in the
					// xml file, but this isn't a functional concern.
					for (Node node : fn.getInboundDependencies()) {
						// indent().append("<ref s=\"" + node.getName() + "\" t=\"" + fe.getId() + "\"/>").eol();

						AbstractElement se = _stringToElementMap.get(node.getName());

						if (se != null && se instanceof MethodElement) {
							MethodElement source = (MethodElement) se;

							if (!source.getReferences().contains(fe))
								source.getReferences().add(fe);
						}
					}
				}
			}

			// inherits
			for (AbstractElement ae : _elementToNodeMap.keySet()) {
				if (ae instanceof ClassElement) {
					ClassElement ce = (ClassElement) ae;

					if (ce.getParents().size() > 0) {
						for (ClassElement pe : ce.getParents()) {
							if (!pe.getId().equals("java.lang.Object")) {
								// ignore Object inheritance relationships
								// indent().append("<inh p=\"" + pe.getId() + "\" c=\"" + ce.getId() + "\"/>").eol();
							}
						}
					}
				}
			}

			for (AbstractElement abse : _elementToNodeMap.keySet()) {
				if (abse instanceof AnnotatableElement) {
					AnnotatableElement ae = (AnnotatableElement) abse;

					Node fn = _elementToNodeMap.get(ae);
					for (Node dep : fn.getOutboundDependencies()) {
						if (dep instanceof FeatureAnnotationNode) {

							String annotationId = ((FeatureAnnotationNode) dep).getName();
							// String targetId = ae.getId();
							// indent().append("<ann a=\"" + annotationId + "\" " + kind + "=\"" + targetId + "\"/>").eol();

							ClassElement ann = (ClassElement) _stringToElementMap.get(annotationId);

							if (ann == null) {
								System.err.println("ERROR: Unable to load annotation: " + annotationId);
							}

							if (!ae.getAnnotations().contains(ann))
								ae.getAnnotations().add(ann);
						}
					}
				}
			}

			atTopLevel = false;
		}
	}

	public Model getModel() {
		return _model;
	}

	protected void preprocessPackageNode(PackageNode node) {
		super.preprocessPackageNode(node);
	}

	protected void postprocessPackageNode(PackageNode node) {
		if (shouldShowPackageNode(node)) {
		}
	}

	public void visitInboundPackageNode(PackageNode node) {
		printInboundNode(node, "package");
	}

	public void visitOutboundPackageNode(PackageNode node) {
		printOutboundNode(node, "package");
	}

	// Hashtable<String, ClassElement> _classes = new Hashtable<String, ClassElement>();
	Hashtable<AbstractElement, Node> _elementToNodeMap = new Hashtable<AbstractElement, Node>();
	Hashtable<String, AbstractElement> _stringToElementMap = new Hashtable<String, AbstractElement>();

	protected void preprocessClassNode(ClassNode node) {
		super.preprocessClassNode(node);

		String name = node.getName();

		ClassElement ce = null;
		if (!_model.hasClass(name)) {

			boolean isExternal = !node.isConfirmed();
			boolean isInterface = node.isInterface();
			boolean isAbstract = node.isAbstract();

			if (!isExternal) {
				ce = new ClassElement(name, isExternal, isInterface, isAbstract);
			} else {
				ce = new ClassElement(name, isExternal);
			}
			ce.setVisibilty(node.getVisibility());
			_model.addElement(ce);

			if (VERBOSE_OUTPUT)
				System.out.println("Pre class: " + ce);

		} else {
			ce = _model.getClass(name);
		}

		for (ClassNode parent : node.getParents()) {
			if (VERBOSE_OUTPUT)
				System.out.println("\t parent: " + parent.getName());

			String lName = parent.getName();
			ClassElement parentElement = null;

			if (!_model.hasClass(lName)) {
				boolean lIsExternal = !parent.isConfirmed();
				boolean lIsInterface = parent.isInterface();
				boolean lIsAbstract = parent.isAbstract();

				if (lIsExternal) {
					parentElement = new ClassElement(lName, lIsExternal);
				} else {
					parentElement = new ClassElement(lName, lIsExternal, lIsInterface, lIsAbstract);
				}
				parentElement.setVisibilty(parent.getVisibility());

				_model.addElement(parentElement);
			}

			parentElement = _model.getClass(lName);
			ce.getParents().add(parentElement);
		}

		for (ClassNode child : node.getChildren()) {
			if (VERBOSE_OUTPUT)
				System.out.println("\t child: " + child.getName());
		}

		for (FeatureNode feature : node.getFeatures()) {

			if (feature instanceof FeatureMethodNode) {
				if (VERBOSE_OUTPUT)
					System.out.println("\t method: " + feature.getName());

				if (!_model.hasMethod(feature.getName())) {
					MethodElement me = new MethodElement(feature.getName());
					_model.addElement(me);
				}

				MethodElement me = _model.getMethod(feature.getName());
				if (!ce.getMethods().contains(me)) {
					ce.getMethods().add(me);
				}

				me.setVisibilty(feature.getVisibility());

				addElement(me, feature);
				// _stringToElementMap.put(me.getId(), me);
				// _elementToNodeMap.put(me, feature);

				FeatureMethodNode n = (FeatureMethodNode) feature;
				if (n.getReturnTypeName() != null && !_model.hasClass(n.getReturnTypeName())) {
					// NOTE: the 'true' is just a guess here
					_model.addElement(new ClassElement(n.getReturnTypeName(), true));
				}

				if (n.getReturnTypeName() != null) {
					me.setReturn(new MethodReturnElement(_model.getClass(n.getReturnTypeName())));
				} else {
					System.out.println("\t\tNull return type: "+n);
				}

				for (int i = 0; i < n.getParameters().size(); i++) {
					String pType = n.getParameters().get(i);

					if (!_model.hasClass(pType)) {
						// NOTE: true is just a guess here
						_model.addElement(new ClassElement(pType, true));
					}

					me.getParameters().add(i, new MethodParamElement(_model.getClass(pType), i));
				}

			} else if (feature instanceof FeatureFieldNode) {
				if (VERBOSE_OUTPUT)
					System.out.println("\t field: " + feature.getName() + " num outbound: " + feature.getOutboundDependencies().size());

				String fieldName = feature.getName();
				ClassElement fieldTypeElement = null;

				if (feature.isConfirmed()) {

					if (feature.getOutboundDependencies().size() != 1) {
						String msg = "WARN: field: " + fieldName + " does not seem to have a type";
						// System.err.println("\t\t" + msg);

						Logger.getLogger(getClass()).warn(msg);

						// this should be somewhere that's not in this loop
						String unknownType = "::UnknownType::";
						if (!_model.hasClass(unknownType)) {
							_model.addElement(new ClassElement(unknownType, true));
						}
						fieldTypeElement = _model.getClass(unknownType);

					} else {

						ClassNode fieldTypeNode = (ClassNode) feature.getOutboundDependencies().iterator().next();
						String fieldTypeName = fieldTypeNode.getName();

						if (!_model.hasClass(fieldTypeName)) {

							boolean lIsExternal = !fieldTypeNode.isConfirmed();
							boolean lIsInterface = fieldTypeNode.isInterface();
							boolean lIsAbstract = fieldTypeNode.isAbstract();

							if (lIsExternal) {
								fieldTypeElement = new ClassElement(fieldTypeName, lIsExternal);
							} else {
								fieldTypeElement = new ClassElement(fieldTypeName, lIsExternal, lIsInterface, lIsAbstract);
							}
							fieldTypeElement.setVisibilty(fieldTypeNode.getVisibility());
							_model.addElement(fieldTypeElement);
						}
						fieldTypeElement = _model.getClass(fieldTypeName);
					}
				}
				FieldElement fe = null;

				if (!_model.hasField(fieldName)) {
					_model.addElement(new FieldElement(fieldName, !feature.isConfirmed(), fieldTypeElement));
				}

				fe = _model.getField(fieldName);
				fe.setVisibilty(feature.getVisibility());
				addElement(fe, feature);
				// _elementToNodeMap.put(fe, feature);

				ce.getFields().add(fe);
			} else if (feature instanceof FeatureAnnotationNode) {
				// These are hanled below in preprocessFeatureNode (not sure if that is the right place or not)

				ClassElement ann = new ClassElement(feature.getName(), true);
				ann.setVisibilty(feature.getVisibility());
				addElement(ann, feature);

			} else {
				String msg = "ERROR: unhandled feature type: " + feature.getClass();
				// System.err.println(msg);
				Logger.getLogger(getClass()).warn(msg);
			}

		}
		if (shouldShowClassNode(node)) {
			// indent().append("<class confirmed=\"").append(node.isConfirmed() ? "yes" : "no").append("\">").eol();
			// raiseIndent();
			// indent().printScopeNodeName(node).eol();
		}
	}

	private void addElement(AbstractElement element, Node node) {
		_elementToNodeMap.put(element, node);
		_stringToElementMap.put(element.getId(), element);
	}

	protected void postprocessClassNode(ClassNode node) {

		if (VERBOSE_OUTPUT)
			System.out.println("Post class: " + node.getName());

		if (shouldShowClassNode(node)) {
			lowerIndent();
			// indent().append("</class>").eol();
		}
	}

	public void visitInboundClassNode(ClassNode node) {
		printInboundNode(node, "class");
	}

	public void visitOutboundClassNode(ClassNode node) {
		printOutboundNode(node, "class");
	}

	protected void preprocessFeatureNode(FeatureNode node) {
		super.preprocessFeatureNode(node);

		if (node instanceof FeatureAnnotationNode) {
			// ensure any annotation types are in the _classes data structure
			// NOTE: this might not be needed but for now we'll just be safe
			String name = ((FeatureAnnotationNode) node).getName();
			boolean isExternal = !node.isConfirmed();

			ClassElement ce = null;

			if (!_model.hasClass(name)) {
				if (!isExternal) {
					ce = new ClassElement(name, isExternal, false, false);
				} else {
					ce = new ClassElement(name, isExternal);
				}
				_model.addElement(ce);
			}
		}

		if (shouldShowFeatureNode(node)) {

		}
	}

	protected void postprocessFeatureNode(FeatureNode node) {
		if (shouldShowFeatureNode(node)) {
		}
	}

	public void visitInboundFeatureNode(FeatureNode node) {
		printInboundNode(node, "feature");
	}

	public void visitOutboundFeatureNode(FeatureNode node) {
		printOutboundNode(node, "feature");
	}

	public void printInboundNode(Node node, String type) {
		if (isShowInbounds()) {
		}
	}

	public void printOutboundNode(Node node, String type) {
		if (isShowOutbounds()) {
		}
	}

	protected Printer printScopeNodeName(Node node, String name) {
		super.printScopeNodeName(node, name);

		return this;
	}
}
