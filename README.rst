
-------------------------
DepFind_UW
-------------------------

We are attempting to extend [depfind.sf.net] to extract the static call graph for a system at build time using ant. While this works currently, we are looking at a few edge cases that aren't currently handled by depfind. Depfind analyzes compiled java code (either in ``jar`` files or ``.class`` files) and writes the results to an XML file.

Generating static data from class files
--------------------------------------------------

``DependencyExtractor -verbose -out <out.xml> -xml <dir tree of class files>``

Generating static data from a jar
--------------------------------------------------

``DependencyExtractor -verbose -out <out.xml> -xml <jar file>``

Debugging DepFind_UW
-------------------------

``DependencyExtractor`` has a main function that can be provided command line arguments such as the ones below:

* ``-verbose -xml /Users/rtholmes/Workspaces/inconsistencyWorkspace/analysisbenchmark/bin/sample/``
* These args make the output verbose, prints the XML to screen, and points the analysis at a specific folder hierarchy full of ``.class`` files.

This technique enables you to easily set breakpoints and quickly work with the code without having to deal with building the JAR and debugging via ant.  

Building a JAR
-------------------------

#. cd ``depfind_uw/DependencyFinder/``
#. ``ant jar`` (make sure the system builds the jar successfully)
#. ``ant iiDeploy`` (deploy the jar to the ``inconsistencyinspectorresources`` project)
#. run the AnalysisBenchmark suite to make sure new jar works as the old one did
#. commit and push the ``inconsistencyinspectorresources`` project
